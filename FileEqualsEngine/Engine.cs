﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileEqualsEngine
{
    /// <summary>
    /// 
    /// </summary>
    public enum CompareMethodType
    {
        HashMD5 = 1,
        HashSHA512 = 2,
        Byte2Byte = 3
    };

    public interface IFileConditionEqualComparable
    {
        List<EqualFiles> FindEqualConditionFiles(EqualFiles SameLengthFiles);
    }

    /// <summary>
    /// Статический класс для предоставления доступа к общим данным
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// </summary>
        public const string SEPARATOR_OperationType = ";";

        public const string ArchiveDirectoryName = "CompressedData";
        public const string UnarchiveDirectoryName = "UncompressedData";

        public const string ArchiveFileName = "TargetArchive.zip";
    }

    public static class MainOperations
    {

        public static bool ByteArrayIsEqual(byte[] EtalonArray, byte[] ComparedArray)
        {
            byte tmp, etalonByte, comparedByte;

            for (int i = 0; i < EtalonArray.Length; i++)
            {
                etalonByte = EtalonArray[i];
                comparedByte = ComparedArray[i];
                tmp = (byte)(etalonByte & comparedByte);
                //tmp = EtalonArray[i] & ComparedArray[i];
                if (etalonByte != tmp)
                    return false;
            }

            return true;
        }

        public static string GetCompareMethods()
        {
            string ret = "";

            ret += String.Format("Методы сравнения содержимого:\n" +
                                 CompareMethodType.HashMD5 + "=  {0}" + Constants.SEPARATOR_OperationType +
                                 CompareMethodType.HashSHA512 + "=  {1}" + Constants.SEPARATOR_OperationType +
                                 CompareMethodType.Byte2Byte + "=  {2}" + Constants.SEPARATOR_OperationType
                                 ,
                                 (int)CompareMethodType.HashMD5,
                                 (int)CompareMethodType.HashSHA512,
                                 (int)CompareMethodType.Byte2Byte
                                );

            return ret;
        }

    }

    public class Md5FileConditionComparer : IFileConditionEqualComparable
    {
        List<FileInfo> sendedSameLengthFiles = new List<FileInfo>();
        List<HashStore> fileHashes = new List<HashStore>();
        List<EqualFiles> findedEqualConditionFiles = new List<EqualFiles>();

        public List<EqualFiles> FindEqualConditionFiles(EqualFiles SameLengthFiles)
        {
            sendedSameLengthFiles = SameLengthFiles.Files;
            findedEqualConditionFiles = new List<EqualFiles>(); // обнуляем список возвращаемых значений на случай повторного вызова
            //CalculateAllHashes();
            CheckEqualCondition();

            return findedEqualConditionFiles;
        }

        private void CalculateAllHashes()
        {
            FileStream curFileStream = null;
            byte[] hashEtalon;
            fileHashes = new List<HashStore>();
            MD5 md5Hasher = MD5.Create();
            HashStore curHashStore;

            fileHashes = new List<HashStore>();

            for (Int32 i = 0; i < sendedSameLengthFiles.Count; i++)
            {
                try
                {
                    curFileStream = new FileStream(sendedSameLengthFiles[i].FullName, FileMode.Open);
                }
                catch
                {
                    //sendedSameLengthFiles.Remove(sendedSameLengthFiles[i]);
                    //i -= 1;
                    continue;
                }

                hashEtalon = md5Hasher.ComputeHash(curFileStream);
                curFileStream.Close();

                curHashStore = new HashStore(sendedSameLengthFiles[i], hashEtalon);

                fileHashes.Add(curHashStore);
            }
        }

        //private void CheckEqualCondition()
        //{
        //    MD5 md5Hasher = MD5.Create();

        //    // Крутим пока не останется минимум 2 файла (если 1, то нет смысла анализировать, 1 не может быть равен еще с кем-то, не осталось вариантов)
        //    while (fileHashes.Count > 1)
        //    {
        //        // 
        //        EqualFiles eqF = new EqualFiles();
        //        eqF.Files.Add(fileHashes[0].TargetFile);

        //        // 
        //        for (Int32 i = 1; i < fileHashes.Count; i++)
        //        {
        //            //
        //            if (MainOperations.ByteArrayIsEqual(fileHashes[0].CalculatedHash, fileHashes[i].CalculatedHash) == true)
        //            {
        //                eqF.Files.Add(fileHashes[i].TargetFile);
        //            }
        //        }
        //        // Убиваем все элементы, которые были проанализированы на текущей итерации
        //        foreach (FileInfo curFile in eqF.Files)
        //        {
        //            for (int i = 0; i < fileHashes.Count; i++)
        //            {
        //                if (fileHashes[i].TargetFile == curFile)
        //                {
        //                    fileHashes.Remove(fileHashes[i]);
        //                    i -= 1;
        //                }
        //            }
        //        }
        //        // Если на итерации найдены файлы с одинаковой длиной, то класс сохраняем в списке
        //        if (eqF.Files.Count > 1)
        //        {
        //            findedEqualConditionFiles.Add(eqF);
        //        }
        //    }
        //}

        private void CheckEqualCondition()
        {
            byte[] hashEtalon, hashCompared;
            MD5 md5Hasher = MD5.Create();
            FileStream curFileStream = null, comparedFileStream = null;

            // Крутим пока не останется минимум 2 файла (если 1, то нет смысла анализировать, 1 не может быть равен еще с кем-то, не осталось вариантов)
            while (sendedSameLengthFiles.Count > 1)
            {
                // 
                EqualFiles eqF = new EqualFiles();
                eqF.Files.Add(sendedSameLengthFiles[0]);

                try
                {
                    curFileStream = new FileStream(sendedSameLengthFiles[0].FullName, FileMode.Open);
                }
                catch
                {
                    sendedSameLengthFiles.Remove(sendedSameLengthFiles[0]);
                    continue;
                }

                hashEtalon = md5Hasher.ComputeHash(curFileStream);

                // Первый файл по очереди сравниваем со всеми, начиная со второго в коллекции
                for (Int32 i = 1; i < sendedSameLengthFiles.Count; i++)
                {
                    try
                    {
                        comparedFileStream = new FileStream(sendedSameLengthFiles[i].FullName, FileMode.Open);
                    }
                    catch
                    {
                        continue;
                    }

                    hashCompared = md5Hasher.ComputeHash(comparedFileStream);
                    //
                    if (MainOperations.ByteArrayIsEqual(hashEtalon, hashCompared) == true)
                    {
                        eqF.Files.Add(sendedSameLengthFiles[i]);
                    }
                    comparedFileStream.Close();
                }
                // Убиваем все элементы, которые были проанализированы на текущей итерации
                foreach (FileInfo curFile in eqF.Files)
                {
                    sendedSameLengthFiles.Remove(curFile);
                }
                // 
                if (eqF.Files.Count > 1)
                {
                    findedEqualConditionFiles.Add(eqF);
                }
                curFileStream.Close();
            }
        }
    }

    public class SHA512FileConditionComparer : IFileConditionEqualComparable
    {
        List<FileInfo> sendedSameLengthFiles = new List<FileInfo>();
        List<EqualFiles> findedEqualConditionFiles = new List<EqualFiles>();

        public List<EqualFiles> FindEqualConditionFiles(EqualFiles SameLengthFiles)
        {
            sendedSameLengthFiles = SameLengthFiles.Files;
            findedEqualConditionFiles = new List<EqualFiles>(); // обнуляем список возвращаемых значений на случай повторного вызова
            CheckEqualCondition();

            return findedEqualConditionFiles;
        }

        private void CheckEqualCondition()
        {
            byte[] hashEtalon, hashCompared;
            SHA512Managed sha512Hasher = new  SHA512Managed();
            FileStream curFileStream = null, comparedFileStream = null;

            // Крутим пока не останется минимум 2 файла (если 1, то нет смысла анализировать, 1 не может быть равен еще с кем-то, не осталось вариантов)
            while (sendedSameLengthFiles.Count > 1)
            {
                // На каждой итерации обновляем экземпляр класса с одинаковыми длинами и по умолчанию в него вносим первый файл из оставшейся коллекции (с ним все сравнивается)
                EqualFiles eqF = new EqualFiles();
                eqF.Files.Add(sendedSameLengthFiles[0]);

                try
                {
                    curFileStream = new FileStream(sendedSameLengthFiles[0].FullName, FileMode.Open);
                }
                catch
                {
                    sendedSameLengthFiles.Remove(sendedSameLengthFiles[0]);
                    continue;
                }

                hashEtalon = sha512Hasher.ComputeHash(curFileStream);

                // Первый файл по очереди сравниваем со всеми, начиная со второго в коллекции
                for (Int32 i = 1; i < sendedSameLengthFiles.Count; i++)
                {
                    try
                    {
                        comparedFileStream = new FileStream(sendedSameLengthFiles[i].FullName, FileMode.Open);
                    }
                    catch
                    {
                        continue;
                    }

                    hashCompared = sha512Hasher.ComputeHash(comparedFileStream);
                    // Если длины совпадают, то очередной сравниваемый добавляем в класс одинаковой длины
                    if (/*hashEtalon == hashCompared*/MainOperations.ByteArrayIsEqual(hashEtalon, hashCompared) == true)
                    {
                        eqF.Files.Add(sendedSameLengthFiles[i]);
                    }
                }
                // Убиваем все элементы, которые были проанализированы на текущей итерации
                foreach (FileInfo curFile in eqF.Files)
                {
                    sendedSameLengthFiles.Remove(curFile);
                }
                // Если на итерации найдены файлы с одинаковой длиной, то класс сохраняем в списке
                if (eqF.Files.Count > 1)
                {
                    findedEqualConditionFiles.Add(eqF);
                }
                curFileStream.Close();
            }
        }
    }

    public class ConditionAnalizer
    {
        List<EqualFiles> equalConditionFilesList = new List<EqualFiles>();
        //EqualFiles sendedEqualLengthFiles;
        LengthAnalizer sendedLengthAnalizer;
        IFileConditionEqualComparable comparer;

        public List<EqualFiles> EqualConditionFilesList
        {
            get { return equalConditionFilesList; }
        }

        public ConditionAnalizer(LengthAnalizer AvailableLengthAnalizer, IFileConditionEqualComparable Comparer)
        {
            sendedLengthAnalizer = AvailableLengthAnalizer;
            comparer = Comparer;
        }

        public void FindEqualConditionFiles()
        {
            foreach (EqualFiles eqFile in sendedLengthAnalizer.EqualLengthFilesList)
            {
                equalConditionFilesList.AddRange(comparer.FindEqualConditionFiles(eqFile));
            }
        }

        public string ShowResult()
        {
            String retStr = "";
            Int32 count = 1;

            foreach (EqualFiles eqFiles in equalConditionFilesList)
            {
                retStr += String.Format("{0}\nEqual condition group #{1} \n{2}\n{3}", "-------------------------------", count.ToString(), "-------------------------------", eqFiles.ToString());
                count += 1;
            }


            return retStr;
        }
    }

    public class LengthAnalizer
    {
        List<EqualFiles> equalLengthFilesList = new List<EqualFiles>();
        private List<FileInfo> existedFiles = new List<FileInfo>();
        
        public List<EqualFiles> EqualLengthFilesList
        {
            get { return equalLengthFilesList; }
        }

        public void FindEqualLengthFiles(string DirectoryPath)
        {
            AddAllExistedFiles(DirectoryPath);
            CheckEqualLength();
        }

        private void AddAllExistedFiles(string MainDirectoryPath)
        {
            DirectoryInfo mainDI = new DirectoryInfo(MainDirectoryPath);
            FileInfo[] allDirectoryFiles = new  FileInfo[0];
            
            allDirectoryFiles = mainDI.GetFiles("*.*", SearchOption.AllDirectories);
            existedFiles.AddRange(allDirectoryFiles);
        }

        private void CheckEqualLength()
        {
            // Крутим пока не останется минимум 2 файла (если 1, то нет смысла анализировать, 1 не может быть равен еще с кем-то, не осталось вариантов)
            while (existedFiles.Count > 1)
            {
                // На каждой итерации обновляем экземпляр класса с одинаковыми длинами и по умолчанию в него вносим первый файл из оставшейся коллекции (с ним все сравнивается)
                EqualFiles eqF = new EqualFiles();
                eqF.Files.Add(existedFiles[0]);
                // Первый файл по очереди сравниваем со всеми, начиная со второго в коллекции
                for (Int32 i = 1; i < existedFiles.Count; i++)
                {
                    // Если длины совпадают, то очередной сравниваемый добавляем в класс одинаковой длины
                    if (existedFiles[0].Length == existedFiles[i].Length)
                    {
                        eqF.Files.Add(existedFiles[i]);
                    }
                }
                // Убиваем все элементы, которые были проанализированы на текущей итерации
                foreach(FileInfo curFile in eqF.Files)
                {
                    existedFiles.Remove(curFile);
                }
                // Если на итерации найдены файлы с одинаковой длиной, то класс сохраняем в списке
                if (eqF.Files.Count > 1)
                {
                    equalLengthFilesList.Add(eqF);
                }
            }
        }

        public string ShowResult()
        {
            String retStr = "";
            Int32 count = 1;
            
            foreach (EqualFiles eqFiles in equalLengthFilesList)
            {
                retStr += String.Format("{0}\nEqual length group #{1} (length = {2} bytes)\n{3}\n{4}", "-------------------------------", count.ToString(), eqFiles.Files[0].Length.ToString(), "-------------------------------", eqFiles.ToString());
                count += 1;
            }


            return retStr;
        }
    }

    public class Archivist
    {
        ConditionAnalizer conditionAnalizer;

        public Archivist(ConditionAnalizer ConditionAnalizerObject)
        {
            conditionAnalizer = ConditionAnalizerObject;
        }

        public void Pack()
        {
            DirectoryInfo di = new DirectoryInfo(Application.StartupPath + Path.DirectorySeparatorChar + Constants.ArchiveDirectoryName);
            if (di.Exists == false)
            {
                di.Create();
            }
            FileStream archiveStream = new FileStream(di.FullName + Path.DirectorySeparatorChar + Constants.ArchiveFileName, FileMode.Create, FileAccess.Write);

            using (ZipArchive archiver = new ZipArchive(archiveStream, ZipArchiveMode.Create))
            {
                foreach (EqualFiles eqFile in conditionAnalizer.EqualConditionFilesList)
                {
                    archiver.CreateEntryFromFile(eqFile.Files[0].FullName, eqFile.Files[0].Name);
                }
            }
        }

        public void Unpack()
        {
            DirectoryInfo diCompress;
            diCompress = new DirectoryInfo(Application.StartupPath + Path.DirectorySeparatorChar + Constants.ArchiveDirectoryName);
            if (diCompress.Exists == false)
            {
                throw new DirectoryNotFoundException("Can't find directory with stored archive!");
            }
            DirectoryInfo diDecompress = new DirectoryInfo(Application.StartupPath + Path.DirectorySeparatorChar + Constants.UnarchiveDirectoryName);
            if (diDecompress.Exists == false)
            {
                diDecompress.Create();
            }

            String archiveStream = diCompress.FullName + Path.DirectorySeparatorChar + Constants.ArchiveFileName;
            using (ZipArchive archiver = ZipFile.Open(archiveStream, ZipArchiveMode.Read))
            {
                archiver.ExtractToDirectory(diDecompress.FullName);   
            }
        }
    }
}
