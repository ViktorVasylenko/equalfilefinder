﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEqualsEngine
{
    class HashStore
    {
        FileInfo targetFile;
        byte[] calculatedHash;

        public HashStore(FileInfo TargetFile)
        {
            targetFile = TargetFile;
        }

        public HashStore(FileInfo TargetFile, byte[] Hash)
        {
            targetFile = TargetFile;
            calculatedHash = Hash;
        }

        public FileInfo TargetFile
        {
            get { return targetFile; }
        }

        public byte[] CalculatedHash
        {
            get { return calculatedHash; }
            set { calculatedHash = value; }
        }
    }
}
