﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEqualsEngine
{
    public class EqualFiles
    {
        List<FileInfo> files = new List<FileInfo>();

        public List<FileInfo> Files
        {
            get { return files; }
        }

        public override string ToString()
        {
            String retStr = "";
            Int32 count = 1;

            foreach(FileInfo fi in Files)
            {
                retStr += String.Format("File {0} = {1}\n", count.ToString(), fi.FullName);
                count += 1;
            }

            return retStr;
        }
    }
}
