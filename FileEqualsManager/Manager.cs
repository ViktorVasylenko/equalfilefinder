﻿using FileEqualsEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEqualsManager
{
    public class Manager
    {
        LengthAnalizer lengthAnalizer = new LengthAnalizer();
        ConditionAnalizer conditionAnalizer = null;
        String directoryPath = "";
        IFileConditionEqualComparable comparer;

        public void GetInitialCondition()
        {
            String userPath = "";
            String compareMethod = "";

            // *******************************************************************************
            Console.WriteLine("Enter directory path to find equal files.");
            userPath = Console.ReadLine();

            if (Directory.Exists(userPath) == false)
            {
                throw new ArgumentException(String.Format("Directory '{0}' is not exist!", userPath));
            }

            directoryPath = userPath;

            // *******************************************************************************
            Console.WriteLine("Select files condition compare method:\n");
            Console.WriteLine(MainOperations.GetCompareMethods());
            compareMethod = Console.ReadLine();
            CreateSelectedComparer(compareMethod);
        }

        private void CreateSelectedComparer(string compareMethod)
        {
            int method;
            if(Int32.TryParse(compareMethod,out method)==false)
            {
                throw new ArgumentException("Only integer can be entered! Return select operation!");
            }
            switch (method)
            {
                case (Int32)CompareMethodType.HashMD5:
                    comparer = new Md5FileConditionComparer();
                    break;
                case (Int32)CompareMethodType.HashSHA512:
                    comparer = new SHA512FileConditionComparer();
                    break;
                case (Int32)CompareMethodType.Byte2Byte:
                    throw new NotImplementedException("Byte to byte compare method is not implemented yet! Sorry.");
                default:
                    throw new ArgumentException("You select unsupported compare method!");
            }
            
        }

        public void CheckFilesByLength()
        {
            lengthAnalizer.FindEqualLengthFiles(directoryPath);
        }

        public void CheckFilesByCondition()
        {

            conditionAnalizer = new ConditionAnalizer(lengthAnalizer, comparer);
            conditionAnalizer.FindEqualConditionFiles();

        }

        public void CheckFilesByCondition(IFileConditionEqualComparable Comparer)
        {
            comparer = Comparer;

            conditionAnalizer = new ConditionAnalizer(lengthAnalizer, comparer);
            conditionAnalizer.FindEqualConditionFiles();

        }

        public void ShowEqualLengthFiles()
        {
            String result = " ***************** Group of files, that length is equal ***************** \n";
            result += lengthAnalizer.ShowResult();

            Console.WriteLine(result);
        }

        public void ShowEqualConditionFiles()
        {
            String result = " ***************** Group of files, that condition is equal ***************** \n";
            result += conditionAnalizer.ShowResult();

            Console.WriteLine(result);
        }

        public void PackData()
        {
            if (conditionAnalizer == null)
                return;

            Archivist arch = new Archivist(conditionAnalizer);
            arch.Pack();
        }

        public void UnackData()
        {
            if (conditionAnalizer == null)
                return;

            Archivist arch = new Archivist(conditionAnalizer);
            arch.Unpack();
        }
    }
}
