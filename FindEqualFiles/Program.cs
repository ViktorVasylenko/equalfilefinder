﻿using FileEqualsManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using FileEqualsEngine;

namespace FindEqualFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch(); 
            Manager manager = new Manager();
            TimeSpan md5Span, sha512Span;
            
            try
            {
                // ************************** Стандартный запуск
                manager.GetInitialCondition();
                manager.CheckFilesByLength();
                manager.ShowEqualLengthFiles();
                Console.WriteLine("\n---------------------------------------------------------\n");
                manager.CheckFilesByCondition();
                manager.ShowEqualConditionFiles();

                //Console.ReadKey(true);
                //// ************************** С замером времени
                //manager.GetInitialCondition();
                //manager.CheckFilesByLength();

                //timer.Reset();
                //timer.Start();
                //manager.CheckFilesByCondition(new SHA512FileConditionComparer());
                //timer.Stop();
                //sha512Span = timer.Elapsed;

                //timer.Start();
                //manager.CheckFilesByCondition(new Md5FileConditionComparer());
                //timer.Stop();
                //md5Span = timer.Elapsed;

                //Console.WriteLine(String.Format("Compare time by using MD5 comparer = '{0}'\nCompare time by using SHA512 comparer = '{1}'", md5Span.ToString(), sha512Span.ToString()));

                // ************************** Архивирование
                Console.WriteLine("Starting compression...");
                manager.PackData();
                Console.WriteLine("Compression done. Starting decompression...");
                manager.UnackData();
                Console.WriteLine("Decompression done.");
                Console.ReadKey(true);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }

            Console.ReadKey(true);
        }
    }
}
